#/bin/sh

cd /opt/dev && \
cd owrt.sysrepo.firewall_plugin && \
mkdir build && cd build && \
cmake -DUSE_CONNTRACK=OFF .. && \
make && \
cp lib/sysrepo-plugin-itmh-* /usr/local/lib/sysrepo/plugins/ && \
ls /opt/dev/data_model && \
sysrepoctl --install=/opt/dev/data_model/itmh-cpe-firewall@2021-07-17.yang && \
sysrepoctl -c itmh-cpe-firewall -e source-mac-support && \
sysrepoctl -c itmh-cpe-firewall -e firewall-ipv4 && \
sysrepoctl -c itmh-cpe-firewall -e firewall-ipv6 && \
sysrepoctl -c itmh-cpe-firewall -e conntrack-flush && \
cd ../ && \
cp files/bin/fw3 /sbin/fw3 && \
chmod +x /sbin/fw3

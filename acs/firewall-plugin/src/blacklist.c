#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <string.h>
#include <stdlib.h>

#include "utils.h"
#include "blacklist.h"
#include "common.h"

/**
 * @brief Frees the blacklist structure member.
 *
 * @param[in] bl Pointer to the blacklist table.
 */
static void free_bl_val(blacklist_tbl_t *bl) {
    if (NULL == bl)
        return;
    free_val(&bl->mac);
    free_val(&bl->section);
    if (NULL != bl)
        free(bl);
}

/**
 * @brief Clears the blacklist table.
 *
 * @param[in] bl Pointer to the blacklist table.
 */
void cleanup_bl (blacklist_tbl_t **bl) {
    blacklist_tbl_t *tmp = *bl, *n_tmp;
    for (; tmp != NULL; tmp = n_tmp) {
        n_tmp = tmp->next;
        free_bl_val(tmp);
    }

    *bl = NULL;
}

/**
 * @brief Print the blacklist table.
 *
 * @param[in] bl Pointer to the blacklist table.
 */
void dump_bl (blacklist_tbl_t *bl) {
    blacklist_tbl_t *tmp = bl;
    DBG_MSG("bl tbl:");
    DBG_MSG("|  № |        mac        |  section  |");
    for (int i = 0; NULL != tmp; tmp = tmp->next, i++)
        DBG("| %2d | %s | %s |", i, tmp->mac, tmp->section);
}

/**
 * @brief Adds an item to the blacklist table.
 *
 * @param[in] bl Pointer to the blacklist table.
 * @param[in] mac Mac address to add to the table.
 * @param[in] section Uci section for identifying the mac belonging to the service.
 */
void add_bl_item (blacklist_tbl_t **bl, char *mac, char *section) {
    blacklist_tbl_t *last = *bl;
    blacklist_tbl_t *tmp = (blacklist_tbl_t *) malloc(sizeof(blacklist_tbl_t));
    tmp->next = NULL;

    int len = strlen(mac) + 1;
    tmp->mac = malloc(len);
    strncpy(tmp->mac, mac, len);

    len = strlen(section) + 1;
    tmp->section = malloc(len);
    strncpy(tmp->section, section, len);

    if (*bl == NULL) {
        *bl = tmp;
        return;
    }

    blacklist_tbl_t *prev = last;
    for (last = *bl; last != NULL; last = last->next) {
        if (string_eq(last->mac, mac)) {
            free_bl_val(tmp);
            return;
        }
        prev = last;
    }

    prev->next = tmp;
    return;
}

/**
 * @brief Сonverts the blacklist table to a delimited with '#' linear string containing mac addresses.
 *
 * @param[in] bl Pointer to the blacklist table.
 * @param[out] str Generated string, allocated dynamically.
 */
static void convert_bl_to_str (blacklist_tbl_t *bl, char **str) {
    *str = (char *) malloc(2);
    strcpy(*str, "#");
    for (blacklist_tbl_t *tmp = bl; NULL != tmp; tmp = tmp->next) {
        char *buf = strdup(*str);
        *str = (char *) realloc(*str, strlen(*str) + 1 + strlen(tmp->mac) + 2);
        sprintf(*str, "%s%s#", buf, tmp->mac);
        free(buf);
    }
}

/**
 * @brief Finds an item in the blacklist table by section and removes it if it exists.
 *
 * @param[in] bl Pointer to the blacklist table.
 * @param[in] section The section of the requested blacklist item. 
 */
void del_bl_item (blacklist_tbl_t **bl, char *section) { /* Быстрее будет выпизднуть целый сервис mac-id-afi */ 
    blacklist_tbl_t *last = *bl;
    blacklist_tbl_t *prev = last;

    if (NULL == last)
        return;

    if (string_eq(last->section, section)) {
        *bl = last->next;
        free_bl_val(last);
        return;
    }

    for (; last != NULL; last = last->next) {
        if (string_eq(last->section, section)) {
            if (NULL != last->next) {
                prev->next = last->next;
                free_bl_val(last);
                last = prev;
            }
            else {
                prev->next = NULL;
                free_bl_val(last);
            }
            return;
        }
        prev = last;
    }

    return;
}

/**
 * @brief Sends a string containing mac addresses to the igmpproxy unix-socket.
 * 
 * @note In case of failure, only a warning will appear. The plugin will not stop its work.
 *
 * @param[in] bl Pointer to the blacklist table.
 */
void send_bl_to_igmpproxy(blacklist_tbl_t *bl) {

    struct sockaddr_un usock_name;

    int usock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (usock < 0) {
        WRN_MSG("Failed to open socket!");
        goto exit;
    }

    usock_name.sun_family = AF_UNIX;
    strncpy(usock_name.sun_path, SOCK_NAME, sizeof (usock_name.sun_path));

    char *blacklist_str = NULL;

    convert_bl_to_str(bl, &blacklist_str);
    DBG("msg to send: %s", blacklist_str);

    if (connect(usock, (struct sockaddr *) &usock_name, sizeof (struct sockaddr_un)) < 0) {
        WRN_MSG("Failed connect to igmpproxy socket!");
        goto exit;
    }

    if (send(usock, blacklist_str, strlen(blacklist_str), 0) < 0) {
        WRN_MSG("Failed send msg!");
        goto exit;
    }

exit: 
    free_val(&blacklist_str);

    if (usock >= 0)
        close(usock);

    return;
}
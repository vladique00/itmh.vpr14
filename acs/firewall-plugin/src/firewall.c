#include <sysrepo/xpath.h>
#include <sysrepo/values.h>
#include <uci.h>

#include <string.h>

#include "firewall.h"
#include "utils.h"
#include "blacklist.h"
#include "service.h"
#include "rule.h"
#include "common.h"
#include "mapping.h"

#ifdef USE_CONNTRACK
void clean_exceptions();
int add_exception(char *address);
int flush_conntrack();
#endif

static int firewall_rpc_cb(sr_session_ctx_t *session, const char *xpath, 
            const sr_val_t *input, const size_t input_cnt,
            sr_event_t event, uint32_t request_id, 
            sr_val_t **output, size_t *output_cnt, void *private_data)
{
    ctx_t *ctx = (ctx_t *) private_data;
    int rc = SR_ERR_OK;
    int len = -1;
    int flush = 0;
    int changes = 0;
    rule_tbl_t *rtbl = NULL;

    service_t *service = (service_t *) calloc(1, sizeof(service_t));

    for (int i = 0; i < input_cnt; i++) {
        if (input[i].type != SR_LIST_T && input[i].type != SR_CONTAINER_T && input[i].type != SR_CONTAINER_PRESENCE_T) {

            sr_xpath_ctx_t state = {0, 0, 0, 0};

            if (sr_xpath_node_name_eq(input[i].xpath, "service-id") || sr_xpath_node_name_eq(input[i].xpath, "rule-id"))
                continue;

            else if (0 == strncmp("exception", sr_xpath_node_name(input[i].xpath), 9)) {
                if (string_eq(sr_xpath_node_idx(input[i].xpath, 1, &state), "conntrack-flush"))
                {
                    char *address = sr_val_to_str(&input[i]);
                    DBG("EXCEPTION ADDR: %s", address);
#ifdef USE_CONNTRACK
                    if(!add_exception(address))
                        ERR("Add exception %s error.", address);
                    if (address != NULL)
                        free(address);
#endif
                    continue;
                }
                sr_xpath_recover(&state);
            }

            else if (sr_xpath_node_name_eq(input[i].xpath, "enable")) {
                if (string_eq(sr_xpath_node_idx(input[i].xpath, 1, &state), "conntrack-flush"))
                {
                    char *enable = sr_val_to_str(&input[i]);

                    if (string_eq(enable, "true")) {
                        flush = 1;
                    }

                    DBG("CONNTRACK FLUSH is: %s", enable);

                    if (enable != NULL)
                        free(enable);

                    continue;
                }
                sr_xpath_recover(&state);
            }

            else if (sr_xpath_node_name_eq(input[i].xpath, "name")) {
                if (sr_xpath_node_name_eq(input[i - 1].xpath, "service-id")) {

                    changes++;

                    if (rtbl != NULL)
                        cleanup_rtbl(&rtbl);

                    /* NEW SERVICE CREATING */
                    free_service(service);
                    service = (service_t *) calloc(1, sizeof(service_t));
                    CHECK_NULL_MSG(service, &rc, error, "service allocation failed");

                    char *afi = (NULL != sr_xpath_node(input[i].xpath, "firewall-ipv4-config", &state)) ? "ipv4" : "ipv6";
                    len = 5; // [ipvX\n]
                    service->afi = malloc(len);
                    strncpy(service->afi, afi, len);

                    service->id = sr_val_to_str(&input[i - 1]);
                    service->name = sr_val_to_str(&input[i]);
                    
                    DBG("NEW SERVICE: %s - %s:%s", service->afi, service->id, service->name);

                    /* CHECK STBL ENTRY */
                    int afi_int = -1;
                    sscanf(service->afi, "ipv%d", &afi_int);
                    service_tbl_t *stbl = lookup_stbl_item(&ctx->stbl, atoi(service->id), afi_int);
                    if (NULL != stbl) {
                        DBG("old service %s-%s cleanup", service->afi, service->id);
                        del_uci_service(ctx, service);
                    } else {
                        service_tbl_t *item = (service_tbl_t *) calloc(1, sizeof(service_tbl_t));
                        item->id = atoi(service->id);
                        sscanf(service->afi, "ipv%d", &item->afi);
                        item->pos = -1;
                        item->num_rules = 0;
                        add_stbl_item(&ctx->stbl, item);
                        free(item);
                    }

                } else if (sr_xpath_node_name_eq(input[i - 1].xpath, "rule-id")) {

                    /* NEW RULE CREATING */
                    free_rule(service->rule);    
                    service->rule = (rule_t *) calloc(1, sizeof(rule_t));
                    CHECK_NULL_MSG(service->rule, &rc, error, "service->rule allocation failed");

                    service->rule->id = sr_val_to_str(&input[i - 1]);
                    service->rule->name = sr_val_to_str(&input[i]);

                    len = strlen(service->id) + strlen(service->name) + strlen(service->afi) + strlen(service->rule->id) + strlen(service->rule->name) + 5; // [':'x4\n]
                    service->rule->code = malloc(len); // sid:s_name:afi:rid:r_name
                    CHECK_NULL_MSG(service->rule->code, &rc, error, "malloc failed");
                    snprintf(service->rule->code, len, "%s:%s:%s:%s:%s", service->id, service->name, service->afi, service->rule->id, service->rule->name);

                    /* LOOKUP RULE TYPE/ACTION/SRC_ZONE/PROTO */
                    int j = i + 1;
                    int action_id = -1;
                    while (j < input_cnt && !sr_xpath_node_name_eq(input[j].xpath, "name")) {
                        if (sr_xpath_node_name_eq(input[j].xpath, "source-zone"))
                            service->rule->src_zone = sr_val_to_str(&input[j]);

                        else if (sr_xpath_node_name_eq(input[j].xpath, "protocol"))
                            service->rule->proto = sr_val_to_str(&input[j]);

                        else if (sr_xpath_node_name_eq(input[j].xpath, "nat-ipv4-action") || sr_xpath_node_name_eq(input[j].xpath, "nat-ipv6-action")) {
                            service->rule->type = strdup("redirect");
                            action_id = j;

                        } else if (sr_xpath_node_name_eq(input[j].xpath, "filter-action")) {
                            service->rule->type = strdup("rule");
                            action_id = j;
                        }

                        if (NULL != service->rule->proto && NULL != service->rule->type && NULL != service->rule->src_zone)
                            break;

                        j++;
                    }

                    service->rule->action = sr_val_to_str(&input[action_id]);

                    /* CREATE UCI SECTION */

                    char *uci_path = NULL;

                    new_uci_path_key("firewall.%s", service->rule->type, &uci_path);
                    struct uci_section *section = set_uci_section(ctx, uci_path);
                    free_val(&uci_path);

                    int afi_n = -1, id = -1;
                    sscanf(service->afi, "ipv%d", &afi_n);
                    id = atoi(service->id);
                    int last_pos = add_stbl_rule(&ctx->stbl, id, afi_n);

                    int shift = add_rtbl_item(&rtbl, atoi(service->rule->id));
                    reorder_uci_section(ctx, section, last_pos - shift);

                    len = strlen(section->e.name) + 1; 
                    service->rule->uci_section = malloc(len);
                    strncpy(service->rule->uci_section, section->e.name, len);
                    
                    new_uci_path_key("firewall.%s.name", service->rule->uci_section, &uci_path);
                    set_uci_option(ctx, uci_path, service->rule->code);
                    free_val(&uci_path);

                    new_uci_path_key("firewall.%s.family", service->rule->uci_section, &uci_path);
                    set_uci_option(ctx, uci_path, service->afi);
                    free_val(&uci_path);

                    if (NULL == service->rule->proto)
                        service->rule->proto = strdup("all");

                    new_uci_path_key("firewall.%s.proto", service->rule->uci_section, &uci_path);
                    set_uci_option(ctx, uci_path, service->rule->proto);
                    free_val(&uci_path);

                    if (string_eq(service->rule->type, "redirect")) {
                        new_uci_path_key("firewall.%s.reflection", service->rule->uci_section, &uci_path);
                        set_uci_option(ctx, uci_path, "0");
                        free_val(&uci_path);
                    }

                    DBG("  NEW RULE: %s:%s:%s", service->rule->id, service->rule->name, service->rule->type);
                }
            } else {

                /* test blacklist conditions */
                if (sr_xpath_node_name_eq(input[i].xpath, "source-mac") &&
                    string_eq(service->rule->src_zone, "lan") && 
                    string_eq(service->rule->action, "drop") &&
                    string_eq(service->afi, "ipv4")) 
                {
                    char *mac = sr_val_to_str(&input[i]);
                    add_bl_item(&ctx->bl_tbl, mac, service->rule->uci_section);
                    free_val(&mac);
                }

                for (int j = 0; j < ctx->map_size_options; j++) {

                    char *xpath = NULL;
                    new_xpath_key(ctx->map_options[j].node, service->afi, NULL, NULL, &xpath);

                    if (sr_xpath_node_name_eq(input[i].xpath, xpath)) {
                        
                        char *uci_path = NULL;
                        new_uci_path_key(ctx->map_options[j].uci_path, service->rule->uci_section, &uci_path);

                        rc = ctx->map_options[j].sr_cb(ctx, service, uci_path, &input[i]);
                        free_val(&uci_path);
                        CHECK_RET(rc, error, "failed map_option %s", sr_strerror(rc));

                    }
                    free_val(&xpath);
                }
            }
        }
    }

    dump_stbl(&ctx->stbl);
    dump_bl(ctx->bl_tbl);

    if (changes) {
        rc = commit_uci_config(ctx);
        CHECK_RET(rc, error, "error uci commit: %s", sr_strerror(rc));

        send_bl_to_igmpproxy(ctx->bl_tbl);

        rc = apply_rules();
        CHECK_RET(rc, error, "error applying rules: %s", sr_strerror(rc));
    }

#ifdef USE_CONNTRACK
    if (flush) {
        if(!flush_conntrack())
            ERR_MSG("flush_conntrack is failed.");
        clean_exceptions();
    }
#endif

error:
    free_service(service);

    return rc;
}

static int firewall_state_cb(sr_session_ctx_t *session, const char *module_name,
                         const char *no_xpath, const char *request_xpath,
                         uint32_t request_id, struct lyd_node **parent,
                         void *private_data)
{
    int rc = SR_ERR_OK;
    ctx_t *ctx = (ctx_t *) private_data;
    
    const char *uci_sections[] = {"rule", "redirect", 0};
    struct uci_element *e = NULL;
    struct uci_section *s;
    char *uci_path = NULL;
    char *uci_value =NULL;
    char *xpath = NULL;

    char *current_service_name = "NULL";
    char *current_afi = "NULL";

    if (*parent == NULL) {
        const struct ly_ctx *ly_ctx = sr_get_context(sr_session_get_connection(session));
        CHECK_NULL_MSG(ly_ctx, &rc, cleanup,"sr_get_context error: libyang context is NULL");
        *parent = lyd_new_path(NULL, ly_ctx, "/itmh-cpe-firewall:firewall", NULL, 0, 0);
    }

    uci_foreach_element(&(ctx->package)->sections, e) {
        s = uci_to_section(e);
        const char **section = uci_sections;
        while (*section != 0) {
            if (string_eq(s->type, (char *) *section)) {            

                new_uci_path_key("firewall.%s.name", s->e.name, &uci_path);
                rc = get_uci_item(ctx->uctx, uci_path, &uci_value);
                free_val(&uci_path);
                CHECK_RET(rc, cleanup, "failed get rule target: %s", sr_strerror(rc));

                service_t *service = (service_t *) calloc(1, sizeof(service_t));
                service->rule = (rule_t *) calloc(1, sizeof(rule_t));

                get_part_uci_name(uci_value, SERVICE_ID, &service->id);
                get_part_uci_name(uci_value, SERVICE_NAME, &service->name);
                get_part_uci_name(uci_value, AFI, &service->afi);
                get_part_uci_name(uci_value, RULE_ID, &service->rule->id);
                get_part_uci_name(uci_value, RULE_NAME, &service->rule->name);
                free_val(&uci_value);
                service->key = strdup(s->e.name);
                service->rule->type = strdup(s->type);

                new_uci_path_key("firewall.%s.target", s->e.name, &uci_path);
                rc = get_uci_item(ctx->uctx, uci_path, &uci_value);
                free_val(&uci_path);
                CHECK_RET(rc, cleanup, "failed get rule target: %s", sr_strerror(rc));

                int len = strlen(uci_value) + 1;
                service->rule->action = malloc(len);
                strncpy(service->rule->action, uci_value, len);
                free_val(&uci_value);
                
                if(!string_eq(service->name, "system") && !string_eq(service->id, service->name) && !string_eq(service->afi, service->id)) 
                {
                    if(!string_eq(current_service_name, service->name) || !string_eq(service->afi, current_afi)) {
                        current_service_name = service->name;
                        current_afi = service->afi;                           
                        new_xpath_key("/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/name", service->afi, service->id, service->rule->id, &xpath);
                        lyd_new_path(*parent, NULL, xpath, service->name, 0, 0);
                        free_val(&xpath);
                    }

                    //DBG("service_id: %s, service_name: %s, afi: %s, rule_id: %s, rule_name: %s", service->id, service->name, service->afi, service->rule->id, service->rule->name);

                    for (int i = 0; i < ctx->map_size_options; i++) {

                        new_uci_path_key(ctx->map_options[i].uci_path, service->key, &uci_path);
                        CHECK_NULL_MSG(uci_path, &rc, cleanup, "failed to generate uci_path");

                        rc = get_uci_item(ctx->uctx, uci_path, &uci_value);
                        CHECK_RET(rc, cleanup, "failed get rule target: %s", sr_strerror(rc));

                        new_xpath_key(ctx->map_options[i].xpath, service->afi, service->id, service->rule->id, &xpath);
                        CHECK_NULL_MSG(xpath, &rc, cleanup, "failed to generate xpath");

                        rc = ctx->map_options[i].uci_cb(ctx, uci_path, xpath, uci_value, service, *parent);
                        free_val(&uci_value);
                        CHECK_RET_MSG(rc, cleanup, "failed uci->sr translation");

                        free_val(&xpath);
                        free_val(&uci_path);
                    }          
                }
                free_service(service);
            }
            section++;
        }
    }

cleanup:
    free_val(&xpath);
    free_val(&uci_path);
    free_val(&uci_value);
    return rc;
}

static int
init_service_tbl(ctx_t *ctx) 
{
    int rc = SR_ERR_OK;
    char *uci_path = NULL;
    char *uci_value =NULL;
    const char *uci_sections[] = {"rule", "redirect", 0};
    struct uci_element *e = NULL;
    struct uci_section *s;
    int pos = -1;
    uci_foreach_element(&(ctx->package)->sections, e)
    {
        s = uci_to_section(e);
        if (e->type == UCI_TYPE_SECTION)
            pos++;
        const char **section = uci_sections;
        while (*section != 0) {
            if (string_eq(s->type, (char *) *section)) {   
                new_uci_path_key("firewall.%s.name", s->e.name, &uci_path);
                rc = get_uci_item(ctx->uctx, uci_path, &uci_value);
                free_val(&uci_path);
                CHECK_RET(rc, error, "failed get rule name: %s", sr_strerror(rc));

                service_t *service = (service_t *) calloc(1, sizeof(service_t));

                get_part_uci_name(uci_value, SERVICE_ID, &service->id);
                get_part_uci_name(uci_value, SERVICE_NAME, &service->name);
                get_part_uci_name(uci_value, AFI, &service->afi);
                free_val(&uci_value);

                if (!string_eq(service->name, "system")) {
                    //DBG("rule: pos: %d | service: afi: %s, id: %s, name: %s", pos, service->afi, service->id, service->name);
                    service_tbl_t *item = (service_tbl_t *) calloc(1, sizeof(service_tbl_t));
                    item->id = atoi(service->id);
                    sscanf(service->afi, "ipv%d", &item->afi);
                    item->pos = pos;
                    item->num_rules = 1;
                    add_stbl_item(&ctx->stbl, item);
                    free(item);

                    /* test blacklist conditions */
                    if (string_eq(service->afi, "ipv4") && string_eq(s->type, "rule")) {
                        char *mac = NULL;
                        char *target = NULL;

                        new_uci_path_key("firewall.%s.src_mac", s->e.name, &uci_path);
                        get_uci_item(ctx->uctx, uci_path, &mac);
                        free_val(&uci_path);

                        new_uci_path_key("firewall.%s.target", s->e.name, &uci_path);
                        get_uci_item(ctx->uctx, uci_path, &target);
                        free_val(&uci_path);

                        if (NULL != mac && string_eq(target, "DROP"))
                            add_bl_item(&ctx->bl_tbl, mac, s->e.name);

                        free_val(&mac);
                        free_val(&target);
                    }
                }
                free_service(service);
            }
            section++;
        }
    }
    set_last_pos(pos + 1);

    dump_stbl(&ctx->stbl);
    dump_bl(ctx->bl_tbl);

    send_bl_to_igmpproxy(ctx->bl_tbl);

error:
    return rc;
}

int sr_plugin_init_cb(sr_session_ctx_t *session, void **private_ctx)
{
    int rc = SR_ERR_OK;

    INF_MSG("Start plugin initialization");

    ctx_t *ctx = (ctx_t *) calloc(1, sizeof(ctx_t));
    CHECK_NULL_MSG(ctx, &rc, error, "plugin ctx memory allocation error");

    ctx->sub = NULL;
    ctx->sess = session;
    ctx->yang_model = YANG;
    ctx->config_file = "firewall";
    ctx->uctx = NULL;
    ctx->stbl = NULL;
    ctx->bl_tbl = NULL;
    load_sr_uci_mapping(ctx);
    *private_ctx = ctx;
    
    rc = load_uci_ctx(ctx);
    CHECK_RET(rc, error, "error reload uci ctx: %s", sr_strerror(rc));

    rc = init_service_tbl(ctx);
    CHECK_RET(rc, crash, "error load services array: %s", sr_strerror(rc));

    rc = sr_rpc_subscribe(ctx->sess, "/" YANG ":firewall-rpc", firewall_rpc_cb, ctx, 0, SR_SUBSCR_CTX_REUSE, &ctx->sub);
    CHECK_RET(rc, error, "failed sr_rpc_subscribe: %s", sr_strerror(rc));

    rc = sr_oper_get_items_subscribe(ctx->sess, YANG, "/" YANG ":firewall", firewall_state_cb, ctx, SR_SUBSCR_CTX_REUSE, &ctx->sub);
    CHECK_RET(rc, error, "failed sr_oper_get_items_subscribe: %s", sr_strerror(rc));

    INF_MSG("Plugin initialization was successful!");
    return SR_ERR_OK;

error:
    ERR("Plugin initialization failed: %s", sr_strerror(rc));
    if (NULL != ctx->sub) {
        sr_unsubscribe(ctx->sub);
        ctx->sub = NULL;
    }
    return rc;

crash:
    ERR_MSG("Corrupted uci config detected! Reset firewall uci config!");
    rc = load_uci_ctx(ctx);
    CHECK_RET(rc, error, "error reload uci ctx: %s", sr_strerror(rc));

    clean_firewall_rules(ctx);

    rc = commit_uci_config(ctx);
    CHECK_RET(rc, error, "error uci commit: %s", sr_strerror(rc));

    rc = apply_rules();
    CHECK_RET(rc, error, "error applying rules: %s", sr_strerror(rc));

    rc = SR_ERR_OPERATION_FAILED;

    goto error;
}

void sr_plugin_cleanup_cb(sr_session_ctx_t *session, void *private_ctx)
{
    INF("Plugin cleanup called, private_ctx is %s available.", private_ctx ? "" : "not");
    if (NULL == private_ctx)
        return;

    ctx_t *ctx = (ctx_t *) private_ctx;
    if (NULL == ctx)
        return;

    if (NULL != ctx->sub)
        sr_unsubscribe(ctx->sub);
    if (NULL != ctx->uctx)
        uci_free_context(ctx->uctx);

    cleanup_stbl(&ctx->stbl);
    cleanup_bl(&ctx->bl_tbl);

    free(ctx);

    INF_MSG("Plugin cleaned-up successfully");
}

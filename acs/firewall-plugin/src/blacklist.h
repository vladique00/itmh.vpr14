#ifndef BLACKLIST_H
#define BLACKLIST_H

#define SOCK_NAME "/var/run/sysrepo-igmpproxy.sock"

typedef struct blacklist_tbl_s {
    char *section;
    char *mac;
    struct blacklist_tbl_s *next;
} blacklist_tbl_t;

void cleanup_bl (blacklist_tbl_t **bl);
void dump_bl(blacklist_tbl_t *bl);
void add_bl_item (blacklist_tbl_t **bl, char *mac, char *section);
void del_bl_item (blacklist_tbl_t **bl, char *section);
void send_bl_to_igmpproxy(blacklist_tbl_t *bl);

#endif /* BLACKLIST_H */

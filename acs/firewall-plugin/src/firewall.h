#ifndef __FIREWALL_H__
#define __FIREWALL_H__

#include <sysrepo.h>
#include "uci.h"

#include "service.h"
#include "blacklist.h"
#include "mapping.h"

typedef struct ctx_s {
    const char *yang_model;
    const char *config_file;
    struct uci_context *uctx;
    struct uci_package *package;
    sr_session_ctx_t *sess;
    sr_subscription_ctx_t *sub;
    sr_uci_mapping_t *map_options;
    int map_size_options;
    service_tbl_t *stbl;
    blacklist_tbl_t *bl_tbl;
    const char *uci_sections[];
} ctx_t;

typedef struct rule_s {
    char *action;
    char *id;
    char *type;
    char *proto;
    char *name;
    char *src_zone;
    char *code;
    char *uci_section;
} rule_t;

typedef struct service_s {
    char *id;
    char *afi;
    char *name;
    char *key;
    rule_t *rule;
} service_t;

#endif /* __FIREWALL_H__ */

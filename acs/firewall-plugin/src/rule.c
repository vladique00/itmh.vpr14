#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "rule.h"
#include "common.h"

/**
 * @brief Clears the rules table.
 *
 * @param[in] rtbl Pointer to the rule table.
 */
void cleanup_rtbl (rule_tbl_t **rtbl) {
    DBG_MSG("Cleanup rtbl.");
    rule_tbl_t *tmp = *rtbl, *n_tmp;
    for (; tmp != NULL; tmp = n_tmp) {
        n_tmp = tmp->next;
        free(tmp);
    }
    *rtbl = NULL;
}

/**
 * @brief Print the rules table.
 *
 * @param[in] rtbl Pointer to the rule table.
 */
void dump_rtbl(rule_tbl_t **rtbl){
    rule_tbl_t *tmp = *rtbl;
    DBG_MSG("rule tbl:");
    DBG_MSG("|  № | id |");
    for (int i = 0; tmp != NULL; tmp = tmp->next, i++)
        DBG("| %2d | %3d | %3d |", i, tmp->id);
}

/**
 * @brief Adds an item to the rules table.
 *
 * @param[in] rtbl Pointer to the rule table.
 * @param[in] id ID of the current rule.
 * @return Shift from last position.
 */
int add_rtbl_item(rule_tbl_t **rtbl, int id) {
    rule_tbl_t *last = *rtbl;

    int inserted = 0;
    int shift = 0;

    rule_tbl_t *tmp = (rule_tbl_t *) malloc(sizeof(rule_tbl_t));
    tmp->id = id;
    tmp->next = NULL;

    if (*rtbl == NULL) {
        *rtbl = tmp;
        return 0;
    }

    rule_tbl_t *prev = last;
    for (last = *rtbl; last != NULL; last = last->next) {
        if (inserted) {
            shift++;
        } else {
            if (last->id > id) {
                if (prev == last) {
                    tmp->next = last;
                    *rtbl = tmp;
                } else {
                    prev->next = tmp;
                    tmp->next = last;
                }
                inserted = 1;
            }
        }

        prev = last;
    }

    if (inserted)
        return shift + 1;
    else {
        prev->next = tmp;
        return 0;
    }
}

#ifndef __MAPPING_H__
#define __MAPPING_H__

#include <sysrepo.h>

typedef struct ctx_s ctx_t;
typedef struct service_s service_t;

struct uci_section *set_uci_section(ctx_t *ctx, char *uci_path);
int reorder_uci_section(ctx_t *ctx, struct uci_section *s, int pos);
int set_uci_option(ctx_t *ctx, char *uci_path, char *value);
int get_uci_item(struct uci_context *uctx, char *uci_path, char **value);
int del_uci_item(ctx_t *ctx, char *uci_path);
int load_uci_ctx(ctx_t *ctx);
int commit_uci_config(ctx_t *ctx);
void del_uci_service(ctx_t *ctx, service_t *service);

int sr_option_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_proto_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_name_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_zone_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_nat_ip_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_nat_port_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);
int sr_option_target_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value);

int uci_option_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_name_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_proto_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_zone_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_nat_ip_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_nat_port_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);
int uci_option_target_cb (ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent);

typedef int (*sr_callback) (struct ctx_s *ctx, struct service_s *service, char *uci_path, const sr_val_t *value);
typedef int (*uci_callback) (struct ctx_s *ctx, char *uci_path, char *xpath, char *value, struct service_s *service, struct lyd_node *parent);

typedef struct sr_uci_mapping_s {
    sr_callback sr_cb;
    uci_callback uci_cb;
    char *uci_path;
    char *node;
    char *xpath;
} sr_uci_mapping_t;

void load_sr_uci_mapping(ctx_t *ctx);

#endif /* __MAPPING_H__ */
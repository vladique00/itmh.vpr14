#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "common.h"

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

static struct nfct_handle *cth, *ith;

#define MAX_TMPL 128
static struct nf_conntrack *tmpl[MAX_TMPL];
static int tmpl_num = 0;

union ct_address {
	uint32_t v4;
	uint32_t v6[4];
} ct_address;

struct addr_parse {
	struct in_addr addr;
	struct in6_addr addr6;
	unsigned int family;
};

enum ct_direction {
	DIR_SRC = 0,
	DIR_DST = 1
};

static const int famdir2attr[2][2] = {
	{ ATTR_ORIG_IPV4_SRC, ATTR_ORIG_IPV4_DST },
	{ ATTR_ORIG_IPV6_SRC, ATTR_ORIG_IPV6_DST }
};

int add_exception(char *address);
void clean_exceptions();
int flush_conntrack();

static int tmpl_cmp(struct nf_conntrack *ct);
static int tmpl_add(char *addr, enum ct_direction dir);
static int parse_addr(const char *cp, union ct_address *address);
static int parse_inetaddr(const char *cp, struct addr_parse *parse);

static int parse_inetaddr(const char *cp, struct addr_parse *parse)
{
	if (inet_aton(cp, &parse->addr))
		return AF_INET;
	else if (inet_pton(AF_INET6, cp, &parse->addr6) > 0)
		return AF_INET6;
	return AF_UNSPEC;
}

static int parse_addr(const char *cp, union ct_address *address)
{
	char buf[INET6_ADDRSTRLEN];
	struct addr_parse parse;
	int family;

	strncpy((char *) &buf, cp, INET6_ADDRSTRLEN);
	buf[INET6_ADDRSTRLEN - 1] = '\0';

	family = parse_inetaddr(buf, &parse);
	switch (family) {
	case AF_INET:
		address->v4 = parse.addr.s_addr;
		break;
	case AF_INET6:
		memcpy(address->v6, &parse.addr6, sizeof(parse.addr6));
		break;
	}

	return family;
}

static int tmpl_add(char *addr, enum ct_direction dir)
{
	union ct_address ad;
	const int l3protonum = parse_addr(addr, &ad);
	if (l3protonum == AF_UNSPEC) {
		ERR("Invalid IP address %s", addr);
		return 0;
	}

	DBG("%s -> %d", addr, l3protonum);

	if (tmpl_num == MAX_TMPL) {
		ERR_MSG("Limit addresses");
		return 0;
	}

	tmpl[tmpl_num++] = nfct_new();
	if (tmpl[tmpl_num - 1] == NULL) {
		ERR_MSG("nfct_new()");
		return 0;
	}

	if (l3protonum == AF_INET) {
		nfct_set_attr_u8(tmpl[tmpl_num - 1], ATTR_L3PROTO, AF_INET);
		nfct_set_attr_u32(tmpl[tmpl_num - 1], famdir2attr[l3protonum == AF_INET6][dir], ad.v4);
	}
	else if (l3protonum == AF_INET6) {
		nfct_set_attr_u8(tmpl[tmpl_num - 1], ATTR_L3PROTO, AF_INET6);
		nfct_set_attr(tmpl[tmpl_num - 1], famdir2attr[l3protonum == AF_INET6][dir], ad.v6);
	}

	return 1;
}

static int tmpl_cmp(struct nf_conntrack *ct)
{
	for (int i = 0; i < tmpl_num; i++)
		if (nfct_cmp(ct, tmpl[i], NFCT_CMP_ORIG))
			return 1;

	return 0;
}

static int data_cb(enum nf_conntrack_msg_type type,
				struct nf_conntrack *ct,
				void *data)
{
	char buf[4096];
	int res = 0;

	nfct_snprintf(buf, sizeof(buf), ct, NFCT_T_UNKNOWN, NFCT_O_DEFAULT, 0);

	if (tmpl_cmp(ct)) {
		DBG("# SAVE   -> %s\n", buf);
	}
	else {
		DBG("- DELETE -> %s\n", buf);

		res = nfct_query(ith, NFCT_Q_DESTROY, ct);
		if (res < 0)
			WRN("nfct_query: NFCT_Q_DESTROY: %d", res);
	}

	return NFCT_CB_CONTINUE;
}

/**
 * @brief Adds an exception to the list of templates for subsequent cleanup.
 *
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int add_exception(char *address)
{
    if (!tmpl_add(address, DIR_SRC) ||
        !tmpl_add(address, DIR_DST))
	{
		ERR("Add exception %s is failed.", address);
		return 0;
	}

    return 1;
}

/**
 * @brief Сreates a connection to the conntrack subsystem,
 * clears the records according to the specified templates.
 */
void clean_exceptions()
{
    for (int i = 0; i < tmpl_num; i++)
		nfct_destroy(tmpl[i]);

	tmpl_num = 0;
}

/**
 * @brief Clears the static list of exception templates.
 *
 * @return Return code (1 on success, 0 on failure).
 */
int flush_conntrack() 
{
    int ret = 1;

	ith = nfct_open(CONNTRACK, 0);
	cth = nfct_open(CONNTRACK, 0);

	if (!cth || !ith) {
		ERR_MSG("Can't open handler");
		return 0;
	}

	nfct_callback_register(cth, NFCT_T_ALL, data_cb, NULL);

	struct nfct_filter_dump *filter_dump = nfct_filter_dump_create();

	if (filter_dump == NULL) {
		ERR_MSG("nfct_filter_dump_create error");
		ret = 0;
		goto exit;
	}

	nfct_filter_dump_set_attr_u8(filter_dump,
					     NFCT_FILTER_DUMP_L3NUM,
					     AF_UNSPEC);

	if (nfct_query(cth, NFCT_Q_DUMP_FILTER, filter_dump) < 0)
		WRN_MSG("nfct_query: NFCT_Q_DUMP_FILTER.");

	nfct_filter_dump_destroy(filter_dump);

exit:
	if (cth != NULL)
		nfct_close(cth);
	if (ith != NULL)
		nfct_close(ith);
	return ret;
}

#ifndef RTBL_H
#define RTBL_H

typedef struct rule_tbl_s {
    int id;
    int pos;
    struct rule_tbl_s *next;
} rule_tbl_t;

void cleanup_rtbl (rule_tbl_t **rtbl);
int add_rtbl_item(rule_tbl_t **rtbl, int id);
void dump_rtbl(rule_tbl_t **rtbl);

#endif /* RTBL_H */
#ifndef __COMMON_H__
#define __COMMON_H__

#include <sysrepo.h>

#define YANG "itmh-cpe-firewall"
#define LOG_MARK "[" YANG "]: "

#define ERR(MSG, ...) SRP_LOG_ERR(LOG_MARK MSG, __VA_ARGS__)
#define ERR_MSG(MSG) SRP_LOG_ERRMSG(LOG_MARK MSG)
#define WRN(MSG, ...) SRP_LOG_WRN(LOG_MARK MSG, __VA_ARGS__)
#define WRN_MSG(MSG) SRP_LOG_WRNMSG(LOG_MARK MSG)
#define INF(MSG, ...) SRP_LOG_INF(LOG_MARK MSG, __VA_ARGS__)
#define INF_MSG(MSG) SRP_LOG_INFMSG(LOG_MARK MSG)
#define DBG(MSG, ...) SRP_LOG_DBG(LOG_MARK MSG, __VA_ARGS__)
#define DBG_MSG(MSG) SRP_LOG_DBGMSG(LOG_MARK MSG)

#define CHECK_RET_MSG(RET, LABEL, MSG) \
    do { \
        if (SR_ERR_OK != RET) { \
            ERR_MSG(MSG); \
            goto LABEL; \
        } \
    } while (0)

#define CHECK_RET(RET, LABEL, MSG, ...) \
    do { \
        if (SR_ERR_OK != RET) { \
            ERR(MSG, __VA_ARGS__); \
            goto LABEL; \
        } \
    } while (0)

#define CHECK_NULL_MSG(VALUE, RET, LABEL, MSG) \
    do { \
        if (NULL == VALUE) { \
            *RET = SR_ERR_NOMEM; \
            ERR_MSG(MSG); \
            goto LABEL; \
        } \
    } while (0)

#define CHECK_NULL(VALUE, RET, LABEL, MSG, ...) \
    do { \
        if (NULL == VALUE) { \
            *RET = SR_ERR_NOMEM; \
            ERR(MSG, __VA_ARGS__); \
            goto LABEL; \
        } \
    } while (0)

#define UCI_CHECK_RET_MSG(UCI_RET, SR_RET, LABEL, MSG) \
	do { \
		if (UCI_OK != UCI_RET) { \
			ERR_MSG(MSG); \
            *SR_RET = SR_ERR_INTERNAL; \
            goto LABEL; \
		} \
	} while (0)

#define UCI_CHECK_RET(UCI_RET, SR_RET, LABEL, MSG, ...) \
	do { \
		if (UCI_OK != UCI_RET) { \
			ERR(MSG, __VA_ARGS__); \
            *SR_RET = SR_ERR_INTERNAL; \
			goto LABEL; \
		} \
	} while (0)

#define UCI_CHECK_ITEM(VALUE, RET, LABEL, MSG, ...) \
	do { \
		if (NULL == VALUE) { \
			*RET = SR_ERR_NOT_FOUND; \
			DBG(MSG, __VA_ARGS__); \
			goto LABEL; \
		}\
	} while (0)


#define SET_STR(VAR, DATA) \
    do { \
        if (VAR != NULL) { \
            free(VAR); \
        } \
        if (DATA != NULL) { \
            VAR = strdup(DATA ? DATA : ""); \
        } else { \
            VAR = NULL; \
        } \
    } while (0)

#define SET_MEM_STR(VAR, DATA) \
    do { \
        if (DATA != NULL) { \
            memcpy(VAR, DATA, sizeof(DATA)); \
        } \
    } while (0)

#endif /* __COMMON_H__ */
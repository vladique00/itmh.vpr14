#include <uci.h>
#include <sysrepo.h>
#include <string.h>

#include "firewall.h"
#include "utils.h"
#include "common.h"
#include "service.h"

/**
 * @brief Compares two strings, including the length of the strings.. 
 *
 * @param[in] s1 The first string to compare.
 * @param[in] s2 The second string to compare.
 * @return True in case that the strings are equal, false otherwise
 */
bool string_eq(char *s1, char *s2)
{
    return (0 == strcmp(s1, s2) && (strlen(s1) == strlen(s2))) ? true : false;
}

/**
 * @brief Replaces a substring in a string.
 *
 * @param[in] str Source string.
 * @param[in] orig The substring to replace.
 * @param[in] rep The new replaced substring.
 * @return Pointer to the original string with the replaced substring.
 */
char *replace_substr(char *str, char *orig, char *rep)
{
    char buffer[128];
    char *p;

    while ((p = strstr(str, orig))!=NULL) { 
        strncpy(buffer, str, p - str); 
        buffer[p - str] = '\0';
        sprintf(buffer + (p - str), "%s%s", rep, p + strlen(orig));
        strncpy(str, buffer, strlen(buffer) + 2);
    }

    return str;
}

/**
 * @brief Finds the requested part of uci name. 
 *
 * @param[in] name The name of the rule uci, containing the code in the format service_id:service_name:afi:rule_id:rule_name.
 * @param[in] part Part of the name declared by the enumerator part_name.
 * @param[out] value Requested part of name, allocated dynamically.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_NOMEM on allocation failed).
 */
int get_part_uci_name(const char *name, part_name part, char **value)
{
    int err_code = SR_ERR_OK;
    *value = malloc(strlen(name) + 1);
    CHECK_NULL_MSG(*value, &err_code, cleanup, "malloc failed");

    char *buff = malloc(strlen(name) + 1);
    CHECK_NULL_MSG(buff, &err_code, cleanup, "malloc failed");
    strncpy(buff, name, strlen(name) + 1);

    char delimiter = ':';
    int start_sub = 0;
    int end_sub = 0;
    char *res;

    for (int i = 0; i < part; i++)
    {
        start_sub = end_sub;
        res = strchr(buff + start_sub + 1, delimiter);
        if (res == NULL)
        {
            end_sub = strlen(name) + 1;
            break;
        }
        else 
            end_sub = res - buff + 1;
    }
    buff[end_sub - 1] = '\0';
    strncpy(*value, buff + start_sub, strlen(buff) - start_sub + 1);

cleanup:
    free_val(&buff);
    return err_code;
}

/**
 * @brief Generates a new xpath based on the received afi and keys.
 *
 * @param[in] xpath Original xpath.
 * @param[in] afi Address family identifier.
 * @param[in] key1 The key containing the service id.
 * @param[in] key2 The key containing the rule id.
 * @param[out] new_xpath Generated new xpath, allocated dynamically.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_NOMEM on allocation failed).
 */
int new_xpath_key(char *xpath, char *afi, char *key1, char *key2, char **new_xpath) {
    int rc = SR_ERR_OK;
    int len = 0;

    CHECK_NULL_MSG(xpath, &rc, cleanup, "missing parameter xpath");

    char *buff = (char *) malloc(sizeof(char) * strlen(xpath) + 2);
    CHECK_NULL_MSG(buff, &rc, cleanup, "malloc failed");

    strcpy(buff, xpath);
    buff = replace_substr(buff, "[afi]", afi);

    if (NULL == key1 || NULL == key2) {
        *new_xpath = (char *) malloc(strlen(buff) + 1);
        strcpy(*new_xpath, buff);
        free_val(&buff);
        return rc;
    }

    len = strlen(key1) + strlen(key2) + strlen(buff) + 1;
    *new_xpath = (char *) malloc(len + 1);
    CHECK_NULL_MSG(*new_xpath, &rc, cleanup, "failed malloc");

    snprintf(*new_xpath, len, buff, key1, key2);

cleanup:
    free_val(&buff);
    return rc;
}

/**
 * @brief Generates a new xpath based on the received node.
 *
 * @param[in] xpath Original xpath.
 * @param[in] node Node to be placed at the end of the xpath.
 * @param[out] new_xpath Generated new xpath, allocated dynamically.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_NOMEM on allocation failed).
 */
int new_xpath_node(char *xpath, char *node, char **new_xpath) {
    int rc = SR_ERR_OK;
    int len = -1;
    CHECK_NULL_MSG(xpath, &rc, cleanup, "missing parameter xpath");

    len = strlen(xpath) + strlen(node) + 1;
    char *buff = (char *) malloc(len);
    CHECK_NULL_MSG(buff, &rc, cleanup, "malloc failed");
    strncpy(buff, xpath, len);

    size_t sub = strrchr(buff, '/') - buff + 1;
    buff[sub] = '\0';

    len = strlen(node) + strlen(buff) + 1;
    *new_xpath = (char *) malloc(len);
    CHECK_NULL_MSG(*new_xpath, &rc, cleanup, "failed malloc");
    snprintf(*new_xpath, len, "%s%s", buff, node);

cleanup:
    if (NULL != buff)
    free_val(&buff);
    return rc;
}

/**
 * @brief Generates a new uci path based on the received key.
 *
 * @param[in] uci_path Original uci path.
 * @param[in] key The key containing the uci section.
 * @param[out] new_uci_path Generated new uci path, allocated dynamically.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_NOMEM on allocation failed).
 */
int new_uci_path_key(char *uci_path, char *key, char **new_uci_path) {
    int rc = SR_ERR_OK;
    int len = 0;

    CHECK_NULL_MSG(uci_path, &rc, cleanup, "missing parameter uci_path");

    if (NULL == key) {
        *new_uci_path = strdup(uci_path);
        return rc;
    }

    len = strlen(key) + strlen(uci_path) - 1;

    *new_uci_path = malloc(len);
    CHECK_NULL_MSG(*new_uci_path, &rc, cleanup, "failed malloc");

    snprintf(*new_uci_path, len, uci_path, key);

cleanup:
    return rc;
}

/**
 * @brief Completely removes all non-system firewall rules.
 *
 * @param[in] ctx Pointer to the plugin context.
 */
void clean_firewall_rules(ctx_t *ctx)
{
    const char *uci_sections[] = {"rule", "redirect", 0};
    struct uci_element *e = NULL, *tmp = NULL;
    struct uci_section *s;
    int rc = SR_ERR_OK;
    char *uci_path = NULL;
    char *uci_value =NULL;
    char *key = NULL;
    
    char *uci_path_template = "firewall.%s";
    char *uci_path_template_name = "firewall.%s.name";

    uci_foreach_element_safe(&(ctx->package)->sections, tmp, e)
    {
        s = uci_to_section(e);
        const char **section = uci_sections;
        while(*section != 0) {
            if (string_eq(s->type, (char *)*section)) {
                key = s->e.name;
                new_uci_path_key(uci_path_template_name, key, &uci_path);
                get_uci_item(ctx->uctx, uci_path, &uci_value);

                char *service_name = NULL;
                get_part_uci_name(uci_value, SERVICE_NAME, &service_name);
                
                if(!string_eq(service_name, "system"))
                {
                    new_uci_path_key(uci_path_template, key, &uci_path);
                    //DBG("uci_value to delete: %s", uci_value);
                    rc = del_uci_item(ctx, uci_path);
                    CHECK_RET(rc, cleanup, "del_uci_item %d", rc);
                }
            }
            section++;
        }
    }

cleanup:
    return;
}

/**
 * @brief Apply rules by reloading the firewall.
 *
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_OPERATION_FAILED in case of non-execution of the command or absence of a firewall).
 */
int apply_rules()
{
    int rc = SR_ERR_OK;
    char *command = "/sbin/fw3 reload 1 > /dev/null 2>&1";
    FILE *file;

    if((file = fopen("/sbin/fw3", "r")) != NULL)
    {
        fclose(file);
        if (system(command) != 0)
            rc = SR_ERR_OPERATION_FAILED;
    }
    else
    {
        rc = SR_ERR_OPERATION_FAILED;
        ERR_MSG("/sbin/fw3 not found");
    }

    return rc;
}

/**
 * @brief Frees the value.
 *
 * @param[in] value The value to be freed.
 */
void free_val(char **value) {
    if (NULL == *value) {
        return;
    }
    free(*value);
    *value = NULL;
}

/**
 * @brief Frees the structure of the rule.
 */
void free_rule(rule_t *rule) {
    if (NULL == rule)
        return;
    free_val(&rule->action);
    free_val(&rule->id);
    free_val(&rule->type);
    free_val(&rule->proto);
    free_val(&rule->name);
    free_val(&rule->src_zone);
    free_val(&rule->code);
    free_val(&rule->uci_section);
    free(rule);
    rule = NULL;
}

/**
 * @brief Frees the service structure, excluding the rule structure.
 */
void free_service(service_t *service) {
    if (NULL == service)
        return;
    free_val(&service->id);
    free_val(&service->name);
    free_val(&service->afi);
    free_val(&service->key);
    free_rule(service->rule);
    free(service);
    service = NULL;
}
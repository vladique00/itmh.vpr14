#ifndef STBL_H
#define STBL_H

typedef struct service_tbl_s {
    int id;
    int afi;
    int pos;
    int num_rules;
    struct service_tbl_s *next;
} service_tbl_t;

void cleanup_stbl (service_tbl_t **stbl);
void set_last_pos(int pos);
void add_stbl_item(service_tbl_t **stbl, service_tbl_t *item);
void del_stbl_item(service_tbl_t **stbl, int id, int afi);
service_tbl_t *lookup_stbl_item(service_tbl_t **stbl, int id, int afi);
void dump_stbl(service_tbl_t **stbl);
void del_stbl_rule(service_tbl_t **stbl, int id, int afi);
int add_stbl_rule(service_tbl_t **stbl, int id, int afi);

#endif /* STBL_H */
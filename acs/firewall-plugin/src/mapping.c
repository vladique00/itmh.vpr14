#include "firewall.h"
#include "common.h"
#include "mapping.h"
#include "utils.h"

#include <string.h>
#include <ctype.h>

#include <sysrepo/xpath.h>
#include <sysrepo/values.h>
#include <uci.h>

sr_uci_mapping_t table_sr_uci_options[] = {
    {sr_option_name_cb, uci_option_name_cb,         "firewall.%s.name",        "name",              "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/name"},
    {sr_option_proto_cb, uci_option_proto_cb,       "firewall.%s.proto",       "protocol",          "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/protocol"},
    {sr_option_zone_cb, uci_option_zone_cb,         "firewall.%s.src",         "source-zone",       "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/source-zone"},
    {sr_option_cb, uci_option_cb,                   "firewall.%s.src_ip",      "source-ip",         "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/source-ip"},
    {sr_option_cb, uci_option_cb,                   "firewall.%s.src_port",    "source-port",       "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/source-port"},
    {sr_option_cb, uci_option_cb,                   "firewall.%s.src_mac",     "source-mac",        "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/source-mac"},
    {sr_option_zone_cb, uci_option_zone_cb,         "firewall.%s.dest",        "destination-zone",  "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/destination-zone"},

    // action //
    {sr_option_target_cb, uci_option_target_cb,     "firewall.%s.target",       "filter-action",    "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/filter-action"},
    {sr_option_target_cb, uci_option_target_cb,     "firewall.%s.target",       "nat-[afi]-action", "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/nat-[afi]-action"},
    
    //  nat   //
    {sr_option_nat_ip_cb, uci_option_nat_ip_cb,     "firewall.%s.dest_ip",      "destination-ip",   "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/destination-ip"},
    {sr_option_nat_port_cb, uci_option_nat_port_cb, "firewall.%s.dest_port",    "destination-port", "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/destination-port"},
    {sr_option_nat_ip_cb, uci_option_nat_ip_cb,     "firewall.%s.src_dip",      "nat-[afi]-address","/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/nat-[afi]-address"},
    {sr_option_nat_port_cb, uci_option_nat_port_cb, "firewall.%s.src_dport",    "nat-[afi]-port",   "/itmh-cpe-firewall:firewall/firewall-[afi]-config/service[service-id='%s']/rule[rule-id='%s']/nat-[afi]-port"}
};

/**
 * @brief Loads a map table into the plugin context.
 *
 * @param[in] ctx Pointer to the plugin context.
 */
void load_sr_uci_mapping(ctx_t *ctx) {
    ctx->map_options = table_sr_uci_options;
    ctx->map_size_options =  sizeof(table_sr_uci_options) / sizeof(table_sr_uci_options[0]);
}

/**
 * @brief Removes an item from the uci system.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @param[in] uci_path The uci path to remove an item from uci system.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int del_uci_item(ctx_t *ctx, char *uci_path) {
    int rc = SR_ERR_OK;
    int uci_ret = UCI_OK;
    struct uci_ptr ptr = {0};

    DBG("%s: %s", __func__, uci_path);

    uci_ret = uci_lookup_ptr(ctx->uctx, &ptr, uci_path, true);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "%s: uci_lookup_ptr %d, path %s", __func__, uci_ret, uci_path);
    
    uci_ret = uci_delete(ctx->uctx, &ptr);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "%s: uci_delete %d, path %s", __func__, uci_ret, uci_path);

cleanup:
    return rc;
}

/**
 * @brief Adds a section to the uci system.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @param[in] uci_path The uci path to add section to uci system.
 * @return Added uci section.
 */
struct uci_section *set_uci_section(ctx_t *ctx, char *uci_path) {
    int rc = SR_ERR_OK;
    int uci_ret = UCI_OK;
    struct uci_ptr ptr = {0};
    struct uci_section *section;

    //DBG("UCI SECTION: %s", uci);
    DBG("%s: %s", __func__, uci_path);

    uci_ret = uci_lookup_ptr(ctx->uctx, &ptr, uci_path, true);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "%s: uci_lookup_ptr %d, path %s", __func__, uci_ret, uci_path);

    uci_ret = uci_add_section(ctx->uctx, ctx->package, ptr.section, &section);
    UCI_CHECK_RET(uci_ret, &rc, fail, "%s: uci_add_section %d, path %s", __func__, uci_ret, uci_path);

cleanup:
    return section;

fail:
    ERR("Fail add uci section %s", uci_path);
    return NULL;
}

/**
 * @brief Moves the uci section to the specified position.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @param[in] s Uci section to be moved.
 * @param[in] pos The position to which the uci section will be moved.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int reorder_uci_section(ctx_t *ctx, struct uci_section *s, int pos) {
    int rc = SR_ERR_OK;
    int uci_ret = UCI_OK;

    uci_ret = uci_reorder_section(ctx->uctx, s, pos);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "uci_reorder_section %d, pos %d", uci_ret, pos);

cleanup:
    return rc;
}

/**
 * @brief Adds a option to the uci system.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @param[in] uci_path Path for the uci option.
 * @param[in] value Value for the option.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int set_uci_option(ctx_t *ctx, char *uci_path, char *value) {
    int rc = SR_ERR_OK;
    int uci_ret = UCI_OK;
    struct uci_ptr ptr = {0};

    char *path = malloc(sizeof(char) * (strlen(uci_path) + strlen(value) + 5));
    CHECK_NULL_MSG(path, &rc, cleanup, "malloc failed");

    sprintf(path, "%s=%s", uci_path, value);

    DBG("%s: %s", __func__, path);

    uci_ret = uci_lookup_ptr(ctx->uctx, &ptr, path, true);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "lookup_pointer %d %s", uci_ret, path);

    uci_ret = uci_set(ctx->uctx, &ptr);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "uci_set %d %s", uci_ret, path);

cleanup:
    free_val(&path);
    return rc;
}

/**
 * @brief Saves changes to the uci system and commits to the config file.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int commit_uci_config(ctx_t *ctx) {
    int rc = SR_ERR_OK, uci_ret = UCI_OK;
	char *uci_config_tmp = "firewall";

    DBG("UCI COMMIT: %s", uci_config_tmp);

    uci_ret = uci_save(ctx->uctx, ctx->package);
    UCI_CHECK_RET(uci_ret, &rc, error, "UCI save error %d", uci_ret);

    /* Bad package commit workaround */

	// uci_ret = uci_commit(ctx->uctx, &(ctx->package), true);
    // UCI_CHECK_RET(uci_ret, &rc, error, "uci_commit %d %s", uci_ret, uci_config_tmp);

    FILE *file;
    if ((file = fopen("/etc/config/firewall", "w+")) != NULL)
    {
        uci_ret = uci_export(ctx->uctx, file, ctx->package, true);
        UCI_CHECK_RET(uci_ret, &rc, error, "uci_export %d %s", uci_ret, uci_config_tmp);
        fclose(file);
    }
    else
        ERR_MSG("Error write data to /etc/config/firewall");

    if ((file = fopen("/tmp/.uci/firewall", "w")) != NULL)
        fclose(file);
    else
        ERR_MSG("Error write data to /tmp/.uci/firewall");

error:
    return rc;
}

/**
 * @brief Loads the allocated uci context into the plugin context.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int load_uci_ctx(ctx_t *ctx) {
    int rc = SR_ERR_OK, uci_ret = UCI_OK;

    if (NULL != ctx->uctx)
        uci_free_context(ctx->uctx);

    ctx->uctx = uci_alloc_context();
    if (NULL == ctx->uctx)
        rc = SR_ERR_NOMEM;
    CHECK_RET(rc, error, "Can't allocate uci context: %s", sr_strerror(rc));
    
    uci_ret = uci_load(ctx->uctx, ctx->config_file, &ctx->package);
    UCI_CHECK_RET(uci_ret, &rc, error, "No configuration (package) %s, uci_error %d", ctx->config_file, uci_ret);

error:
    return rc;
}

/**
 * @brief Gets the uci value at the given path.
 *
 * @param[in] uctx Uci context.
 * @param[in] uci_path Path for requested item.
 * @param[out] value Value of requested item, dynamicly allocated.
 * @return Error code (::SR_ERR_OK on success, ::SR_ERR_INTERNAL on failure).
 */
int get_uci_item(struct uci_context *uctx, char *uci_path, char **value) {
    int rc = SR_ERR_OK;
    int uci_ret = UCI_OK;
    struct uci_ptr ptr;
    *value = NULL;

    int len = strlen(uci_path) + 1;
    char *path = (char *) malloc(len);
    CHECK_NULL(path, &rc, cleanup, "malloc %s", uci_path);
    strncpy(path, uci_path, len);

    uci_ret = uci_lookup_ptr(uctx, &ptr, path, true);
    UCI_CHECK_RET(uci_ret, &rc, cleanup, "%s: lookup_pointer %d %s", __func__, uci_ret, path);
    if (NULL == ptr.o || NULL == ptr.o->v.string)
        goto cleanup;

    *value = strdup(ptr.o->v.string);
    CHECK_NULL(*value, &rc, cleanup, "strdup failed for %s", uci_path);

cleanup:
    free_val(&path);

    return rc;
}

/**
 * @brief Removes all rules from the uci system that belong to the service.
 *
 * @param[in] ctx Pointer to the plugin context.
 * @param[in] service Pointer to the service.
 */
void del_uci_service(ctx_t *ctx, service_t *service)
{
    const char *uci_sections[] = {"rule", "redirect", 0};
    struct uci_element *e = NULL, *tmp = NULL;
    struct uci_section *s;
    int rc = SR_ERR_OK;
    char *uci_path = NULL;
    char *uci_value =NULL;
    char *key = NULL;

    uci_foreach_element_safe(&(ctx->package)->sections, tmp, e)
    {
        s = uci_to_section(e);
        const char **section = uci_sections;
        while (*section != 0) {
            if (string_eq(s->type, (char *)*section)) {
                key = s->e.name;
                new_uci_path_key("firewall.%s.name", key, &uci_path);
                get_uci_item(ctx->uctx, uci_path, &uci_value);
                free_val(&uci_path);

                service_t *tmp_service = (service_t *) calloc(1, sizeof(service_t));
                get_part_uci_name(uci_value, SERVICE_ID, &tmp_service->id); // Not safe!
                get_part_uci_name(uci_value, SERVICE_NAME, &tmp_service->name);
                get_part_uci_name(uci_value, AFI, &tmp_service->afi);
                
                if(!string_eq(tmp_service->name, "system"))
                {
                    if (string_eq(service->id, tmp_service->id) && string_eq(service->afi, tmp_service->afi)) {
                        new_uci_path_key("firewall.%s", key, &uci_path);
                        del_bl_item(&ctx->bl_tbl, key);
                        DBG("uci_value to delete: %s", uci_value);
                        rc = del_uci_item(ctx, uci_path);
                        free_val(&uci_path);
                        CHECK_RET(rc, cleanup, "del_uci_item %d", rc);
                        int afi_n = -1;
                        sscanf(tmp_service->afi, "ipv%d", &afi_n);
                        del_stbl_rule(&ctx->stbl, atoi(tmp_service->id), afi_n);
                    }
                }
                free_val(&uci_value);
                free_service(tmp_service);
            }
            section++;
        }
    }

cleanup:
    return;
}

/* Callbacks to convert uci value to sysrepo. */

int uci_option_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    lyd_new_path(parent, NULL, xpath, value, 0, 0);
    return SR_ERR_OK;
}

int uci_option_proto_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    if (string_eq(value, "all"))
        return SR_ERR_OK;;

    lyd_new_path(parent, NULL, xpath, value, 0, 0);
    return SR_ERR_OK;
}

int uci_option_name_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    lyd_new_path(parent, NULL, xpath, service->rule->name, 0, 0);
    return SR_ERR_OK;
}

int uci_option_zone_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    if (NULL != value)
        lyd_new_path(parent, NULL, xpath, value, 0, 0);
    else
        lyd_new_path(parent, NULL, xpath, "device", 0, 0);
    return SR_ERR_OK;
}

int uci_option_nat_ip_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    char *new_xpath = NULL;
    char *node = NULL;

    if ((NULL != strstr(uci_path, "dest_ip")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "DNAT")){
        node = strdup("nat-[afi]-address");
        replace_substr(node, "[afi]", service->afi);
    } else if ((NULL != strstr(uci_path, "src_dip")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "DNAT"))
        node = strdup("destination-ip");

    if (NULL == node)
        new_xpath = strdup(xpath);
    else {
        new_xpath_node(xpath, node, &new_xpath);
        free_val(&node);
    }

    lyd_new_path(parent, NULL, new_xpath, value, 0, 0);
    free_val(&new_xpath);
    return SR_ERR_OK;
}

int uci_option_nat_port_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    char *new_xpath = NULL;
    char *node = NULL;

    if ((NULL != strstr(uci_path, "dest_port")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "DNAT")){
        node = strdup("nat-[afi]-port");
        replace_substr(node, "[afi]", service->afi);
    } else if ((NULL!=strstr(uci_path, "src_dport")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "DNAT"))
        node = strdup("destination-port");

    if (NULL == node)
        new_xpath = strdup(xpath);
    else {
        new_xpath_node(xpath, node, &new_xpath);
        free_val(&node);
    }

    lyd_new_path(parent, NULL, new_xpath, value, 0, 0);
    free_val(&new_xpath);
    return SR_ERR_OK;
}

int uci_option_target_cb(ctx_t *ctx, char *uci_path, char *xpath, char *value, service_t *service, struct lyd_node *parent)
{
    char *new_xpath = NULL;
    char *node = NULL;

    char *s = value;
    while (*s) {
        *s = tolower((unsigned char) *s);
        s++;
    }

    if(string_eq(service->rule->type, "redirect"))
    {
        node = strdup("nat-[afi]-action");
        replace_substr(node, "[afi]", service->afi);
    } 
    else if(string_eq(service->rule->type, "rule"))
        node = strdup("filter-action");

    if (NULL == node)
        return SR_ERR_OK;

    new_xpath_node(xpath, node, &new_xpath);
    free_val(&node);

    lyd_new_path(parent, NULL, new_xpath, value, 0, 0);
    free_val(&new_xpath);
    return SR_ERR_OK;
}

/* Callbacks to convert sysrepo value to uci. */

int sr_option_cb(ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    char *sr_value = sr_val_to_str(value);
    rc = set_uci_option(ctx, uci_path, sr_value);
    free_val(&sr_value);
    CHECK_RET(rc, cleanup, "set_uci_option %x", rc);
cleanup:
    return rc;
}

int sr_option_name_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    rc = set_uci_option(ctx, uci_path, service->rule->name);
    CHECK_RET(rc, cleanup, "failed set_uci_option %x", rc);
cleanup:
    return rc;
}

int sr_option_proto_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    return SR_ERR_OK;
}

int sr_option_zone_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    char *sr_value = sr_val_to_str(value);
    if (string_eq(sr_value, "device")){
        free_val(&sr_value);
        return rc;
    }
    rc = set_uci_option(ctx, uci_path, sr_value);
    free_val(&sr_value);
    CHECK_RET(rc, cleanup, "set_uci_option %x", rc);
cleanup:
    return rc;
}

int sr_option_nat_ip_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    char *sr_value = sr_val_to_str(value);

    if (sr_xpath_node_name_eq(value->xpath, "destination-ip") && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "dnat"))
        replace_substr(uci_path, "dest_ip", "src_dip");
    if ((sr_xpath_node_name_eq(value->xpath, "nat-ipv4-address") || sr_xpath_node_name_eq(value->xpath, "nat-ipv6-address")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "dnat"))
        replace_substr(uci_path, "src_dip", "dest_ip");

    rc = set_uci_option(ctx, uci_path, sr_value);
    free_val(&sr_value);
    CHECK_RET(rc, cleanup, "set_uci_option %x", rc);

cleanup:
    return rc;
}

int sr_option_nat_port_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    char *sr_value = sr_val_to_str(value);

    if (sr_xpath_node_name_eq(value->xpath, "destination-port") && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "dnat"))
        replace_substr(uci_path, "dest_port", "src_dport");
    if ((sr_xpath_node_name_eq(value->xpath, "nat-ipv4-port") || sr_xpath_node_name_eq(value->xpath, "nat-ipv6-port")) && string_eq(service->rule->type, "redirect") && string_eq(service->rule->action, "dnat"))
        replace_substr(uci_path, "src_dport", "dest_port");

    rc = set_uci_option(ctx, uci_path, sr_value);
    free_val(&sr_value);
    CHECK_RET(rc, cleanup, "set_uci_option %x", rc);


cleanup:
    return rc;
}

int sr_option_target_cb (ctx_t *ctx, service_t *service, char *uci_path, const sr_val_t *value)
{
    int rc = SR_ERR_OK;
    char *sr_value = sr_val_to_str(value);
    char *s = sr_value;

    while (*s) {
        *s = toupper((unsigned char) *s);
        s++;
    }

    rc = set_uci_option(ctx, uci_path, sr_value);
    free_val(&sr_value);
    CHECK_RET(rc, cleanup, "set_uci_option %x", rc);

cleanup:
    return rc;
}

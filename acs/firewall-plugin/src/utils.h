#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>

typedef struct ctx_s ctx_t;
typedef struct service_s service_t;
typedef struct rule_s rule_t;

typedef enum part_name_e {
    SERVICE_ID = 1,
    SERVICE_NAME,
    AFI,
    RULE_ID,
    RULE_NAME
} part_name;

int get_part_uci_name(const char *name, part_name part, char **value);
int new_xpath_key(char *xpath, char *afi, char *key1, char *key2, char **new_xpath);
int new_xpath_node(char *xpath, char *node, char **new_xpath);
int new_uci_path_key(char *uci_path, char *key, char **new_uci_path);

bool string_eq(char *first, char *second);
char *replace_substr(char *str, char *orig, char *rep);

void clean_firewall_rules(ctx_t *ctx);
int apply_rules();

void free_val(char **value);
void free_rule(rule_t *rule);
void free_service(service_t *service);

#endif /* UTILS_H */
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "service.h"
#include "common.h"

static int last_pos = -1;

/**
 * @brief Saves the last position of the last rule to write a new service that is currently unknown.
 *
 * @param[in] pos Position of the last rule.
 */
void set_last_pos (int pos) {
    last_pos = pos;
}

/**
 * @brief Clears the services table.
 *
 * @param[in] stbl Pointer to the service table.
 */
void cleanup_stbl (service_tbl_t **stbl) {
    service_tbl_t *tmp = *stbl, *n_tmp;
    for (; tmp != NULL; tmp = n_tmp) {
        n_tmp = tmp->next;
        free(tmp);
    }
    *stbl = NULL;
}

/**
 * @brief Print the services table.
 *
 * @param[in] stbl Pointer to the service table.
 */
void dump_stbl(service_tbl_t **stbl){
    service_tbl_t *tmp = *stbl;
    DBG_MSG("service tbl:");
    DBG_MSG("|  № |  afi |  id | pos | nr |");
    for (int i = 0; tmp != NULL; tmp = tmp->next, i++)
        DBG("| %2d | ipv%d | %3d | %3d | %2d |", i, tmp->afi, tmp->id, tmp->pos, tmp->num_rules);
}

/**
 * @brief Finds an item in the services table by id and afi.
 *
 * @param[in] stbl Pointer to the service table.
 * @param[in] id The id of the requested service. 
 * @param[in] afi The afi of the requested service.
 * @return Pointer to the lookup item, if it exists, NULL otherwise.
 */
service_tbl_t *lookup_stbl_item(service_tbl_t **stbl, int id, int afi) {
    service_tbl_t *tmp = *stbl;
    for (; tmp != NULL; tmp = tmp->next) {
        if (tmp->id == id && tmp->afi == afi)
            return tmp;
    }
    return NULL;
}

/**
 * @brief Adds an item to the services table.
 *
 * @param[in] stbl Pointer to the service table.
 * @param[in] item The pre-initialized item to add. Should be free after use.
 */
void add_stbl_item(service_tbl_t **stbl, service_tbl_t *item) {
    service_tbl_t *last = *stbl;
    service_tbl_t *tmp = (service_tbl_t *) malloc(sizeof(service_tbl_t));
    tmp->next = NULL;
    tmp->id = item->id;
    tmp->afi = item->afi;
    tmp->num_rules = item->num_rules;
    tmp->pos = item->pos;

    if (*stbl == NULL) {
        *stbl = tmp;
        if (tmp->pos == -1)
            tmp->pos = last_pos;
        return;
    }

    service_tbl_t *prev = last;
    for (last = *stbl; last != NULL; last = last->next) {

        if (last->id == item->id && last->afi == item->afi) {
            ++last->num_rules;
            for (last = last->next; last != NULL; last = last->next)
                ++last->pos;
            free(tmp);
            return;
        } else if ((item->id | (item->afi << (sizeof(int) * 8 - 4))) < (last->id | (last->afi << (sizeof(int) * 8 - 4)))) {
            if (prev == last) {
                tmp->next = last;
                *stbl = tmp;
            } else {
                prev->next = tmp;
                tmp->next = last;
                tmp->pos = prev->pos + prev->num_rules;
                tmp->next->pos = tmp->pos + tmp->num_rules;
            }
            
            return;
        } 
        prev = last;
    }

    prev->next = tmp;
    if (-1 == tmp->pos || item->pos != prev->pos + prev->num_rules)
        tmp->pos = prev->pos + prev->num_rules;
    return;
}

/**
 * @brief Finds an item in the services table by id and afi and removes it if it exists.
 *
 * @param[in] stbl Pointer to the service table.
 * @param[in] id The id of the requested service. 
 * @param[in] afi The afi of the requested service.
 */
void del_stbl_item(service_tbl_t **stbl, int afi, int id) {
    service_tbl_t *last = *stbl;
    service_tbl_t *prev = last;

    if (last->id == id && last->afi == afi) {
        *stbl = last->next;
        free(last);
        return;
    }

    for (; last != NULL; last = last->next) {
        if ((last->id == id) && last->afi == afi) {
            if (NULL != last->next) {
                prev->next = last->next;
                free(last);
                last = prev;
            }
            else {
                prev->next = NULL;
                free(last);
            }
            return;
        }
        prev = last;
    }

    return;
}

/**
 * @brief Increments the counter of rules in the found service by id and afi. Shifts the positions of rules further down the table.
 *
 * @param[in] stbl Pointer to the service table.
 * @param[in] id The id of the requested service. 
 * @param[in] afi The afi of the requested service.
 * @return The position at which the rule should be added to the uci.
 */
int add_stbl_rule(service_tbl_t **stbl, int id, int afi) {
    service_tbl_t *tmp = lookup_stbl_item(stbl, id, afi);
    int pos = -1;
    if (NULL != tmp) {
        ++(tmp->num_rules);
        pos = tmp->pos + tmp->num_rules - 1;
    }
    else
        return pos;

    for (tmp = tmp->next; tmp != NULL; tmp = tmp->next)
        ++(tmp->pos);

    return pos;
}

/**
 * @brief Decrements the counter of rules in the found service by id and afi. Shifts the positions of rules further down the table.
 *
 * @param[in] stbl Pointer to the service table.
 * @param[in] id The id of the requested service. 
 * @param[in] afi The afi of the requested service.
 */
void del_stbl_rule(service_tbl_t **stbl, int id, int afi) {
    service_tbl_t *tmp = lookup_stbl_item(stbl, id, afi);
    if (NULL != tmp)
        --tmp->num_rules;
    else
        return;

    for (tmp = tmp->next; tmp != NULL; tmp = tmp->next)
        --tmp->pos;
}
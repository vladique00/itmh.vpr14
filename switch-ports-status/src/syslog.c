//#include "switch-ports-status.h"

#include <syslog.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

static int LogLevel = LOG_WARNING;
static int Log2Stderr = 1;

void setLogLevel(int val) {
    LogLevel = val + LOG_ERR;
}

void setLog2Stderr(int val) {
    Log2Stderr = val;
}

void my_log( int Severity, int Errno, const char *FmtSt, ... )
{
    char LogMsg[128];

    va_list ArgPt;
    unsigned Ln;
    va_start( ArgPt, FmtSt );
    Ln = vsnprintf(LogMsg, sizeof(LogMsg), FmtSt, ArgPt);
    if (Errno > 0)
        snprintf(LogMsg + Ln, sizeof( LogMsg ) - Ln,
                "; Errno(%d): %s", Errno, strerror(Errno));
    va_end( ArgPt );

    if (Severity <= LogLevel) {
        if (Log2Stderr)
            fprintf(stderr, "%s\n", LogMsg);
        else {
            syslog(Severity, "%s", LogMsg);
        }
    }

    if (Severity <= LOG_ERR)
        exit( -1 );
}
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>

#include <syslog.h>
#include <signal.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>

#include <sys/types.h>
#include <arpa/inet.h>

#define __USE_MISC 1

#include <net/if.h>

#include <linux/mtk_netlink.h>

enum {
    GOT_SIGINT = 0x01
};

static int openSocket();
static int waitPortState(int sock, int wan_port);
static int setPortState(char *ifname, int wan_port, int status);

void my_log(int Serverity, int Errno, const char *FmtSt, ...);
void setLogLevel(int val);
void setLog2Stderr(int val);

static void signalHandler(int sig);
static int sighandled = 0;


int main(int argc, char *argv[]) {
    /* Syslog */
    openlog("switch-ports-status", LOG_PID, LOG_USER);

    /* Parse args */
    char ifname[10] = {0};
    int wan_port = -1, flags = 0;

    int opt;
    while ((opt = getopt(argc, argv, "p:i:v:s")) != -1)
    {
        switch (opt)
        {
            case 'p':
                wan_port = atoi(optarg);
                flags |= (1 << 1);
                break;
            case 'i':
                strcpy(ifname, optarg);
                flags |= (1 << 0);
                break;
            case 'v':
                setLogLevel(atoi(optarg));
                break;
            case 's':
                setLog2Stderr(!atoi(optarg));
                break;
            default:
                my_log(LOG_ERR, 0, "Usage: %s [-p wan_port] [-i ifname] [-v verbose 0-4] [-s syslog]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (flags != 3) {
        my_log(LOG_ERR, 0, "Usage: %s [-p wan_port] [-i ifname] [-v verbose] [-s syslog]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    my_log(LOG_DEBUG, 0, "args: ifname: %s, wan_port: %d", ifname, wan_port);

    /* Check interface */
    if (!if_nametoindex(ifname)) {
        my_log(LOG_ERR, 0, "Interface %s not found!", ifname);
        exit(EXIT_FAILURE);
    }

    /* Init syslog/socket/signal */
    struct sigaction sa;
    sa.sa_handler = signalHandler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);

    int state = -1;

    int nl_sk = openSocket();
    if (nl_sk < 0) {
        my_log(LOG_ERR, 0, "Failed to open socket!\n");
        goto exit;
    }

    while (1) {
        if (sighandled) {
            if (sighandled & GOT_SIGINT) {
                sighandled &= ~GOT_SIGINT;
                my_log(LOG_NOTICE, 0, "Got a interrupt signal. Exiting.");
                break;
            }
        }

        state = waitPortState(nl_sk, wan_port);
        if (state >= 0)
            setPortState(ifname, wan_port, state);
    }

exit:
    if (nl_sk >= 0) {
        close(nl_sk);
    }

    return 0;
}

static int openSocket(void)
{
    int sock;
    struct sockaddr_nl addr;
    int group = MYGRP;

    sock = socket(AF_NETLINK, SOCK_RAW, MYPROTO);
    if (sock < 0) {
        my_log(LOG_ERR, 0, "Open AF_NETLINK socket failed.\n");
        return sock;
    }

    memset((void *) &addr, 0, sizeof(addr));
    addr.nl_family = AF_NETLINK;
    addr.nl_pid = getpid();

    if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        my_log(LOG_ERR, 0, "Bind socket failed\n");
        return -1;
    }

    if (setsockopt(sock, 270, NETLINK_ADD_MEMBERSHIP, &group, sizeof(group)) < 0) {
        my_log(LOG_ERR, 0, "setsockopt() failed\n");
        return -1;
    }

    return sock;
}

static int waitPortState (int sock, int wan_port)
{
    struct sockaddr_nl nladdr;
    struct msghdr msg;
    struct iovec iov;
    char *nlbuff = NULL;
    int err, port = -1, val = -1;

    nlbuff = malloc(NETLINK_BUFF_SIZE);
    if (nlbuff == NULL) {
        my_log(LOG_ERR, 0, "Failed malloc nlbuff!\n");
        goto exit;
    }

    iov.iov_base = (void *) nlbuff;
    iov.iov_len = NETLINK_BUFF_SIZE;
    msg.msg_name = (void *) &(nladdr);
    msg.msg_namelen = sizeof(nladdr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    err = recvmsg(sock, &msg, 0);
    if (err < 0) {
        my_log(LOG_ERR, 0, "Received signal or corrupted msg.\n");
        goto exit;
    }
    else {
        err = sscanf(NLMSG_DATA((struct nlmsghdr *) nlbuff), "%d;%d", &port, &val);
        my_log(LOG_DEBUG, 0, "received msg: \"%s\"\n", NLMSG_DATA((struct nlmsghdr *) nlbuff));
    }

    if (err != 2) {
        my_log(LOG_ERR, 0, "corrupted msg: \"%s\"\n", NLMSG_DATA((struct nlmsghdr *) nlbuff));
        goto exit;
    }

    my_log(LOG_DEBUG, 0, "port%d state: %d\n", port, val);

exit:
    if (nlbuff != NULL)
        free(nlbuff);

    return wan_port == port ? val : -1;
}

static int setPortState (char *ifname, int wan_port, int state) {
    char *nlbuff = NULL;
    int nl_sk = -1;
    struct nlmsghdr *nlh;

    nlbuff = malloc(NETLINK_BUFF_SIZE);
    if (nlbuff == NULL) {
        my_log(LOG_ERR, 0, "Failed malloc nlbuff!\n");
        goto exit;
    }

    nl_sk = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (nl_sk < 0) {
        my_log(LOG_ERR, 0, "Failed to open socket!\n");
        goto exit;
    }

    nlh = (struct nlmsghdr *) nlbuff;
    struct ifinfomsg *ifi = NLMSG_DATA(nlh);

    memset(nlbuff, 0, NLMSG_LENGTH(sizeof (*ifi)));
    nlh->nlmsg_len   = NLMSG_LENGTH(sizeof (*ifi));
    nlh->nlmsg_type  = RTM_SETLINK;
    nlh->nlmsg_flags = NLM_F_REQUEST;
    nlh->nlmsg_pid   = getpid();

    ifi->ifi_index = if_nametoindex(ifname);
    ifi->ifi_change |= IFF_UP;
    ifi->ifi_family = AF_UNSPEC;

    if (state)
        ifi->ifi_flags |= IFF_UP;
    else
        ifi->ifi_flags &= ~IFF_UP;

    send(nl_sk, nlbuff, nlh->nlmsg_len, 0);
    my_log(LOG_NOTICE, 0, "WAN port (idx: %d) is %s\n", wan_port, state ? "up" : "down");

exit:
    if (nlbuff != NULL)
        free(nlbuff);

    if (nl_sk >= 0) {
        close(nl_sk);
    }
    return 0;
}

static void signalHandler(int sig) {
    switch (sig) {
    case SIGINT:
    case SIGTERM:
        sighandled |= GOT_SIGINT;
        break;
    }
}
